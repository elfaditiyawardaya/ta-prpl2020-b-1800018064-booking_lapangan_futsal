<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
.judul{
    width: 100%;
	height: 100px;
	float: left;
    text-align: center;
	background-color:yellow;
    text-emphasis-color: black;
    padding: 30px;
}
.menu{
    width: 100%;
	height: 50px;
	float: right;
	background-color:black;
}
.bawah{
	width: 100%;
	background-color: #002933;
    float:left;
    color:white;
    text-align: center;
    font-size: 20px;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
}
.login{
	width: 20%;
	height: 550px;
    float: left;
    color:white;
    background-color: gray;
    padding-top:30px;
    padding-right: 35px;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background-image: url(2.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.tengah{
	width: 80%;
	height: 550px;
    float: right;
    color:black;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background-image: url(1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    padding-top: 50px;
    padding-right: 100px;
    padding-left: 100px;
    background-size: cover;
    text-align: center;
}
h1{
    margin: 60;
    text-align: center;
    font-size: 40px;
    color:black;
    font-weight: bold;
    font-family: 'Verdana';
}
.size{
    font-size:14px;
}
</style>
</head>
<body>
    <div class="judul">
            <h1>APLIKASI BOOKING LAPANGAN FUTSAL</h1>
    </div>
                <div class="menu">
                    
                <header class="header_area" id="header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg align-items-center">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#caviarNav" aria-controls="caviarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
                        <div class="collapse navbar-collapse" id="caviarNav">
                            <ul class="navbar-nav ml-auto" id="caviarMenu">
                                <li class="nav-item">
                                    <a class="nav-link" href="home_admin.php" style="color:white">HOME</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="sewa.php" style="color:white">PENYEWAAN</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="transaksi.php" style="color:white">TRANSAKSI</a>
                                </li>
                            </ul>
                            <div class="caviar-search-btn">
                                <a id="search-btn" href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
                    
</div>
<div class="login">
   
    </div>
</div>
                <div class="tengah">
              <h2>TRANSAKSI</h2>
              <table align=center class="table table-striped ">
				<thead class="thead-dark">
	<tr>
		<th class="size"> No.</th>
		<th class="size"> ID TRANSAKSI </th>
		<th class="size"> ID LAPANGAN</th>
        <th class="size"> KODE SEWA  </th>
        <th class="size"> HARGA TARIF </th>
        <th class="size">  ACTION </th>
	</tr>
	</thead>
    <tbody>
	<?php
	include "connect_db.php";
	$query = mysqli_query($connect,"SELECT *from transaksi");
	$no=1;
	foreach ($query as $data) {?>
		<tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['id_transaksi']; ?></td>
            <td><?php echo $data['id_lapangan']; ?></td>
            <td><?php echo $data['kode_sewa']; ?></td>
            <td><?php echo $data['total_harga']; ?></td>
            <td>
<button type="button" class="btn btn-primary">	<a href="updatetransaksi.php?id_transaksi=<?php echo $data['id_transaksi']?>"style="color:white">Updete</a></button>
<button type="button" class="btn btn-danger">   <a href="hapusdatatransaksi.php?id_transaksi=<?php echo $data['id_transaksi']?>"style="color:white">Hapus</a></button>
	  </td>
		</tr>
        
	<?php }?>
	</tbody>
</table>
<button type="button" class="btn btn-secondary"><a href="tambahtransaksi.php" style="color:white">TAMBAH TRANSAKSI</a></button>
                </div>
            <div class="bawah">
                <p>&copy COPYRIGHT BY I</p>
            </div>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>