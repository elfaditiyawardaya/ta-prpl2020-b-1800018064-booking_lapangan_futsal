<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
.judul{
    width: 100%;
	height: 100px;
	float: left;
    text-align: center;
	background-color:yellow;
    text-emphasis-color: black;
    padding: 30px;
}
.menu{
    width: 100%;
	height: 50px;
	float: right;
	background-color:black;
}
.bawah{
	width: 100%;
	background-color: #002933;
    float:left;
    color:white;
    text-align: center;
    font-size: 20px;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
}
.login{
	width: 20%;
	height: 550px;
    float: left;
    color:white;
    background-color: gray;
    padding-top:30px;
    padding-right: 35px;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background-image: url(2.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.tengah{
	width: 80%;
	height: 550px;
    float: right;
    color:white;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background-image: url(1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    padding-top: 50px;
    padding-right: 125px;
    padding-left: 125px;
    background-size: cover;
    text-align: center;
}
h1{
    margin: 60;
    text-align: center;
    font-size: 40px;
    color:black;
    font-weight: bold;
    font-family: 'Verdana';
}
 
h1{
    text-align: center;
    /*ketebalan font*/
    font-weight: 300;
}
 
.tulisan_login{
    text-align: center;
    /*membuat semua huruf menjadi kapital*/
    text-transform: uppercase;
}
 
.kotak_login{
    width: 250px;
    background: white;
    /*meletakkan form ke tengah*/
    margin: 80px auto;
    padding: 30px 20px;
    border-radius: 20px;
}
 
label{
    font-size: 11pt;
}
 
.form_login{
    /*membuat lebar form penuh*/
    box-sizing : border-box;
    width: 100%;
    padding: 10px;
    font-size: 11pt;
    margin-bottom: 20px;
}
 
.tombol_login{
    background: #46de4b;
    color: white;
    font-size: 11pt;
    width: 100%;
    border: none;
    border-radius: 3px;
    padding: 10px 20px;
}
</style>
</head>
<body>
    <div class="judul">
            <h1>APLIKASI BOOKING LAPANGAN FUTSAL</h1>
    </div>
                <div class="menu">
                    
                <header class="header_area" id="header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg align-items-center">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#caviarNav" aria-controls="caviarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
                        <div class="collapse navbar-collapse" id="caviarNav">
                            <ul class="navbar-nav ml-auto" id="caviarMenu">
                                <li class="nav-item">
                                    <a class="nav-link" href="home_1.php" style="color:white">HOME</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="tamdatapelanggan.php" style="color:white">PENYEWAAN</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="tamdatakaryawan.php" style="color:white">TRANSAKSI</a>
                                </li>
                            </ul>
                            <div class="caviar-search-btn">
                                <a id="search-btn" href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
                    
</div>
<div class="login">
   
    </div>
</div>
        <div class="tengah">
              <h2>JADWAL LAPANGAN</h2>
              <table align=center class="table table-striped ">
				<thead class="thead-dark">
	<tr>
		<th> No.</th>
		<th>  KODE LAPANAGAN </th>
		<th>  NAMA LAPANGAN </th>
	</tr>
    <tbody>
	<?php
	include "connect_db.php";
	$query = mysqli_query($connect,"SELECT *from lapangan");
	$no=1;
	foreach ($query as $data) {?>
		<tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['id_lapangan']; ?></td>
            <td><?php echo $data['nama_lapangan']; ?></td>
		</tr>
       
	<?php }?>
	</tbody>
	</thead>
</table>

                </div>
            <div class="bawah">
                <p>&copy COPYRIGHT BY I</p>
            </div>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>